<?php

namespace Drush\Commands\foundation_starter;

use Drush\Commands\DrushCommands;
use Consolidation\SiteAlias\SiteAliasManagerAwareInterface;
use Consolidation\SiteAlias\SiteAliasManagerAwareTrait;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

/**
 * Foundation Stater Commands.
 */
class FoundationStarterCommands extends DrushCommands implements SiteAliasManagerAwareInterface {

  use SiteAliasManagerAwareTrait;

  /**
   * Drush command that displays the given text.
   *
   * @param string $name
   *   Theme Name.
   * @param string $machine_name
   *   Theme Machine Name.
   * @param string $description
   *   Theme Description.
   *
   * @command foundation_starter:generate-theme
   * @aliases fsgt
   * @usage foundation_starter:generate-theme drupal8
   */
  public function generateTheme($name, $machine_name = NULL, $description = NULL) {
    // Filter everything but letters, numbers, underscores, and hyphens.
    $machine_name = !empty($machine_name) ? preg_replace('/[^a-z0-9_-]+/', '', strtolower($machine_name)) : preg_replace('/[^a-z0-9_-]+/', '', strtolower($name));
    // Eliminate hyphens.
    $machine_name = str_replace('-', '_', $machine_name);
    // Get foundation starter path.
    $self = $this->siteAliasManager()->getSelf();
    $process = $this->processManager()->drush($self, 'drupal:directory', ['foundation_starter']);
    $process->mustRun();
    $starter_path = trim($process->getOutput());
    // Get new theme path.
    $theme_path = explode('/', $starter_path);
    array_pop($theme_path);
    array_pop($theme_path);
    $theme_path = implode('/', $theme_path) . '/custom/' . $machine_name;

    // Make a fresh copy of the theme.
    $filesystem = new Filesystem();
    if ($filesystem->exists(trim($starter_path))) {
      try {
        $filesystem->mirror($starter_path . '/', $theme_path);
      }
      catch (IOExceptionInterface $exception) {
        $this->logger()->error("An error occurred while copying your directory at @path", [
          '@path' => $exception->getPath(),
        ]);
        return;
      }
    }

    // Rename files and fill in the theme machine name.
    $filesystem->rename("$theme_path/foundation_starter.info.yml", "$theme_path/$machine_name.info.yml");
    $filesystem->rename("$theme_path/foundation_starter.breakpoints.yml", "$theme_path/$machine_name.breakpoints.yml");
    $filesystem->rename("$theme_path/foundation_starter.libraries.yml", "$theme_path/$machine_name.libraries.yml");
    $filesystem->rename("$theme_path/foundation_starter.theme", "$theme_path/$machine_name.theme");
    $filesystem->rename("$theme_path/js/foundation-starter.js", "$theme_path/js/$machine_name.js");

    // Change the name of the theme.
    $this->strReplace("$theme_path/$machine_name.info.yml", 'Foundation Starter', "$name");

    // Change the description of the theme.
    if (!empty($description)) {
      $this->strReplace("$theme_path/$machine_name.info.yml", 'A simpler foundation starter theme', $description);
    }

    // Replaces instances of STARTER in required files to name of the theme.
    $this->strReplace("$theme_path/$machine_name.theme", 'foundation starter', "$machine_name");
    $this->strReplace("$theme_path/$machine_name.breakpoints.yml", 'foundation_starter', "$machine_name");
    $this->strReplace("$theme_path/$machine_name.libraries.yml", 'foundation-starter.js', "$machine_name.js");
    $this->strReplace("$theme_path/$machine_name.info.yml", 'foundation_starter', "$machine_name");

    // Removed uneeded files.
    $filesystem->remove([
      "$theme_path/Commands",
      "$theme_path/composer.json",
      "$theme_path/drush.services.yml",
    ]);

    // Notify user of the newly created theme.
    $this->output()->writeln("Theme generated at path: $theme_path.");

    $process = $this->processManager()->drush($self, 'theme:enable', [$machine_name]);
    $process->mustRun();

    // Notify enabled theme.
    $this->output()->writeln("$name enabled.");
    $this->output()->writeln("We are going to uninstall the foundation starter theme, so you can remove it safety.");
    // Uninstalling.
    try {
      $process = $this->processManager()->drush($self, 'theme:uninstall', ['foundation_starter']);
      $process->mustRun();
    }
    catch (\Exception $e) {
      // There is always an error from drush when disabling themes.
    }
  }

  /**
   * Internal helper: Replace strings in a file.
   */
  public function strReplace($file_path, $find, $replace) {
    $file_contents = file_get_contents($file_path);
    $file_contents = str_replace($find, $replace, $file_contents);
    file_put_contents($file_path, $file_contents);
  }

}
