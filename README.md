## CONTENTS OF THIS FILE

 * Introduction
 * Requirements
 * Installation
 * Generating a new theme.
 * Maintainers


## INTRODUCTION

This theme is a foundation zurb starter, you can use it to generate your own theme and then uninstall it.

Foundation Starter is a simpler alternative to existing [ZURB Foundation](https://www.drupal.org/project/zurb_foundation), it does fewer assumptions and just provides you the boilerplate to start your new Drupal theme using Foundation Zurb.

## REQUIREMENTS

  * Drush 9.

## INSTALLATION

  * Install normally as other themes are installed. For support:
    https://www.drupal.org/docs/8/extending-drupal-8/installing-themes

## GENERATING A NEW THEME:

You can use the drush command from site's root as follows:
`drush --include="web/themes/contrib/foundation_starter" foundation_starter:generate-theme MyNewTheme`
You can also set machine name and description
`drush --include="web/themes/contrib/foundation_starter" foundation_starter:generate-theme MyTheme my_theme "This is my theme"`

Your new theme will be placed in themes/custom folder and the foundation starter will be disabled, you can remove it safety after that.

## MAINTAINERS

Current maintainers:
 * Ivan Duarte (jidrone) - https://www.drupal.org/u/jidrone
